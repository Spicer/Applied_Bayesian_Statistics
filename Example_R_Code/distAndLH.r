library(MCMCpack)
# You need MCMCpack to sample from an inverse gamma distribution.  All of the other distributions are availble in R's basic package.

#*** Distributions ***#
p <- c(.25,.5,.75)
n <- 25
y <- 0:25

pdf("binom.pdf")
plot(y, dbinom(y, n, p[1]), type="h", lty=1, lwd=2, ylab="Binomial(25,p)")
lines(y, dbinom(y, n, p[2]), type="h", lty=2, lwd=2, col="red")
lines(y, dbinom(y, n, p[3]), type="h", lty=3, lwd=2, col="blue")
legend("topright", legend = c(.25, .5, .75), title="p", lty=c(1,2,3), col = c("black", "red", "blue"))
dev.off()

lam <- c(1,5,10)
y <- 0:25
pdf("pois.pdf")
plot(y, dpois(y, lam[1]), type="h", lty=1, lwd=2, ylab=expression(Poisson(lambda)))
lines(y, dpois(y, lam[2]), type="h", lty=2, lwd=2, col="red")
lines(y, dpois(y, lam[3]), type="h", lty=3, lwd=2, col="blue")
legend("topright", legend =c(1, 5, 10), title=expression(lambda), lty=c(1,2,3), col=c("black", "red", "blue"))
dev.off()

a <- 2
b <- c(1,5,10)
y <- seq(0,7,length=100)
pdf("gamB.pdf")
plot(y, dgamma(y, a,rate=b[1]), type="l", lty=1, lwd=2, ylim=c(0,5), ylab=expression(paste(Gam(alpha,beta))))
lines(y, dgamma(y,a, rate=b[2]), type="l", lty=2, lwd=2, col="red")
lines(y, dgamma(y, a,rate=b[3]), type="l", lty=3, lwd=2, col="blue")
legend("topright", legend =c(1, 5, 10), title=("alpha=2, beta"), lty=c(1,2,3), col=c("black", "red", "blue"))
dev.off()

a <- c(1,5,10)
b <- 2
y <- seq(0,7,length=100)
pdf("gamA.pdf")
plot(y, dgamma(y, a[1], rate=b), type="l", lty=1, lwd=2, ylim=c(0,2), ylab=expression(paste(Gam(alpha,beta))))
lines(y, dgamma(y,a[2], rate=b), type="l", lty=2, lwd=2, col="red")
lines(y, dgamma(y, a[3],rate=b), type="l", lty=3, lwd=2, col="blue")
legend("topright", legend =c(1, 5, 10), title=("alpha, beta=2"), lty=c(1,2,3), col=c("black", "red", "blue"))
dev.off()

a <- 2
b <- c(1,5,10)
y <- seq(0,7,length=100)
pdf("invgamB.pdf")
plot(y, dinvgamma(y, a,scale=b[1]), type="l", lty=1, lwd=2, ylim=c(0,1.5), ylab=expression(paste(InvGam(alpha,beta))))
lines(y, dinvgamma(y,a, scale=b[2]), type="l", lty=2, lwd=2, col="red")
lines(y, dinvgamma(y, a,scale=b[3]), type="l", lty=3, lwd=2, col="blue")
legend("topright", legend =c(1, 5, 10), title=("alpha=2, beta"), lty=c(1,2,3), col=c("black", "red", "blue"))
dev.off()

a <- c(1,5,10)
b <- 5
y <- seq(0,1,length=100)
pdf("betaA.pdf")
plot(y, dbeta(y, a[1], b), type="l", lty=1, lwd=2, ylab=expression(paste(InvGam(alpha,beta))))
lines(y, dbeta(y,a[2], b), type="l", lty=2, lwd=2, col="red")
lines(y, dbeta(y, a[3],b), type="l", lty=3, lwd=2, col="blue")
legend("topright", legend =c(1, 5, 10), title=("alpha, beta=5"), lty=c(1,2,3), col=c("black", "red", "blue"))
dev.off()

#pdf("allDistsSmall.pdf")
#par(mfrow=c(3,2))
#dev.off()
#pdf("allDists.pdf")
#dev.off()

#*** Examples from Class ***#
#Daisies
muVec <- matrix(seq(-3,6,length=20))
w <- c(2.31, 1.19, 2.48)

#LH function
lhFunc <- function(mu, y, n){
  like <- (2*pi*4)^(-n/2)*exp((-.5/4)*sum( (y-mu)^2))
  return(like)
}
lh <- apply(muVec, 1, lhFunc, w,3)
plot(muVec, lh, type="b", xlab="Mu", ylab="Likelihood")

#First Generation
rhoVec <- matrix(seq(0,1,length=15))
y <- c(1,0,0,0)
lhFunc <- function(theta, x)
{
  like <- (theta^(sum(x))) * ((1-theta)^(sum(1-x)))
  return(like)
}
lh <- apply(rhoVec, 1, lhFunc, y)
plot(rhoVec, lh, ylab="Likelihood", type="b", xlab="rho")





#*** Data and Likelhood ***#
set.seed(6)


#* Bernoulli/binomial Example
#Create data
x <- c(0,1,0,0,1,1,1,0,0,0)
par(mfrow=c(1,1))
hist(x, prob=T, main="data")
mean(x)



#LH function
lhFunc <- function(theta, x){
  like <- (theta^(sum(x))) * ((1-theta)^(sum(1-x)))
  return(like)
}

#evaluate the LH for different values of 
thetaVec <- matrix(seq(0, 1, by=.02))
lh <- apply(thetaVec, 1, lhFunc, x)
plot(thetaVec, lh, ylab="Likelihood", type="b", xlab="theta")


#Compare empirical distribution (histogram) with likelhood
par(mfrow=c(1,3))
hist(x, prob=T, main="Hist of Data", sub=paste("mean=", round(mean(x),2)), xlab="x")  # x can only take 0 and 1
axis(1, label=c(0,1), at=c(0,1))
plot(thetaVec, lh, main="Likelihood", type="b")
abline(v=mean(x), col="red", lwd=2)
plot(c(0,1),c(6,4)/10, main="pmf", type="h",
     lwd=4,sub=paste("mean=", round(mean(x),2)), xlab="x", ylab="Mass", axes=FALSE)
axis(2)
axis(1, at=c(0,1))


#*** Notice properties of the LH
par(mfrow=c(2,2))
plot(thetaVec, lh, main="Likelihood", type="l", lty=1)
abline(v=mean(x), col="red", lwd=2)
abline(h=max(lh), col="blue", lwd=1)
points(mean(x), max(lh), col="green", cex=1, pch=19)

C <- max(lh)
plot(thetaVec, lh+C, main="Likelihood+C", type="l", lty=1)
abline(v=mean(x), col="red", lwd=2)
abline(h=max(lh+C), col="blue", lwd=1)
points(mean(x), max(lh+C), col="green", cex=1, pch=19)

C <- 10^18
plot(thetaVec, lh*C, main="Likelihood*C", type="l", lty=1)# ylim=c(0,4.6e-06))
abline(v=mean(x), col="red", lwd=2)
abline(h=max(lh*C), col="blue", lwd=1)
points(mean(x), max(lh*C), col="green", cex=1, pch=19)

plot(thetaVec, log(lh), main="f(Likelihood); inc. mono.", type="l", lty=1)
abline(v=mean(x), col="red", lwd=2)
abline(h=max(log(lh)), col="blue", lwd=1)
points(mean(x), max(log(lh)), col="green", cex=1, pch=19)



#* Normal Example
y <- rnorm(100, 5, 2)  #collect my data
y <- y[order(y)]       #re-organize, for plotting purposes
par(mfrow=c(1,1))
hist(y, prob=T, main="data")  #histogram of the data
lines(y,dnorm(y,5,2))         #TRUE pdf of the data
mean(y)

#LH function
lhFunc <- function(mu, y, n)
{
  like <- (2*pi*4)^(-n/2)*exp((-.5/4)*sum( (y-mu)^2))
  return(like)
}


muVec <- seq(2, 8, by=.1)
lh <- apply(matrix(muVec), 1, lhFunc, y, 100)
plot(muVec, lh, ylab="Likelihood", type = "b")


#Compare empirical distribution, estimated distribution, and likelihood
par(mfrow=c(1,2))
hist(y, prob=T, main="data", sub=paste("mean=", round(mean(y),2)), ylim=c(0,.25))
lines(y,dnorm(y,5,2), col="blue", lwd=2, lty=1)  #true distribution
mle <- mean(y)
lines(y,dnorm(y,mle,2), col="red", lwd=2, lty=2)  #estimated distribution
plot(muVec, lh, main="Likelihood", type="l")
abline(v=mean(y), col="red", lwd=2)




  
