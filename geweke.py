def _Geweke(samples, intervals=20):

    '''Calculates Geweke Z-Scores'''    

    length=len(samples)/intervals/2

    # discard the first 10 per cent

    first = 0.1*len(samples)

    

    # create empty array to store the results

    z = np.empty(intervals)

    

    for k in np.arange(0, intervals):

        # starting points of the two different subsamples

        start1 = first + k*length

        start2 = len(samples)/2 + k*length

                

        # extract the sub samples

        subsamples1 = samples[start1:start1+length]

        subsamples2 = samples[start2:start2+length]

        

        # calculate the mean and the variance

        mean1 = np.mean(subsamples1)

        mean2 = np.mean(subsamples2)

        var1  = np.var(subsamples1)

        var2  = np.var(subsamples2)

        

        # calculate the Geweke test

        z[k] = (mean1-mean2)/np.sqrt(var1+var2)    

    return z