library(mvtnorm)

mu = c(5,10)
sigma = matrix(c(2,1,1,2), 2, 2)
d = rmvnorm(1000, mu , sigma)

theta1 = d[,1]
theta1_mean = mean(theta1)
print(theta1_mean)
theta1_var = var(theta1)
print(theta1_var)
hist(theta1)

theta2 = d[,2]
theta2_mean = mean(theta2)
print(theta2_mean)
theta2_var = var(theta2)
print(theta2_var)
hist(theta2)