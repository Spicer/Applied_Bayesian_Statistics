rm(list=ls())
library(coda)

#*** Get the Data
dat0 <- read.table("health_ineq_online_table_5_Mod2.csv", sep=",", header=TRUE)
# dat1 <- dat0[,1:4]
# dat2 <- dat0[,5:12]
# dat2 <- scale(dat2)
# dat <- cbind(dat1,dat2)
#** Organize the data
# Maine, Nevada, New Hampshire, New Mexico, South Dakota, West Virginia
le <- dat0[,5]                                     #Response Variable
indicator_val <- 1
maine <- c(rep(indicator_val,14), rep(0,70))
nev <- c(rep(0,14), rep(indicator_val,14), rep(0,56))
nh <- c(rep(0,28), rep(indicator_val,14), rep(0,42))
nm <- c(rep(0,42), rep(indicator_val,14), rep(0,28))
sk <- c(rep(0,56), rep(indicator_val,14), rep(0,14))
wv <- c(rep(0,84))
# wv <- c(rep(0,70), rep(indicator_val,14))

for(col in 6:12)
{
  le <- c(le, dat0[,col])
  maine <- c(maine, rep(indicator_val,14), rep(0,70))
  nev <- c(nev, rep(0,14), rep(indicator_val,14), rep(0,56))
  nh <- c(nh, rep(0,28), rep(indicator_val,14), rep(0,42))
  nm <- c(nm, rep(0,42), rep(indicator_val,14), rep(0,28))
  sk <- c(sk, rep(0,56), rep(indicator_val,14), rep(0,14))
  wv <- c(wv, rep(0,84))
  # wv <- c(wv, rep(0,70), rep(indicator_val,14))
}
inc <- c(rep(1,6*14), rep(2,6*14), rep(3,6*14), rep(4,6*14))
inc <- c(inc, inc)
# inc1 <- c(rep(indicator_val,6*14), rep(0,6*14*3))
# inc1 <- c(inc1, inc1)
inc2 <- c(rep(0,6*14), rep(indicator_val,6*14), rep(0,6*14*2))
inc2 <- c(inc2, inc2)
inc3 <- c(rep(0,6*14*2), rep(indicator_val,6*14), rep(0,6*14))
inc3 <- c(inc3, inc3)
inc4 <- c(rep(0,6*14*3), rep(indicator_val,6*14))
inc4 <- c(inc4, inc4)
gend <- c(rep(0,14*6*4), rep(indicator_val,14*6*4)) #female = 0, male = 1

le <- log(le)
hist(le)
le <- scale(le)
hist(le)
# maine <- scale(maine)
# nev <- scale(nev)
# nh <- scale(nh)
# nm <- scale(nm)
# sk <- scale(sk)
# wv <- scale(wv)
# inc <- log(inc)
inc <- scale(inc)
# gend <- scale(gend)

y <- le[,]
# x <-cbind(maine, nev, nh, nm, sk, inc, gend)
x <-cbind(maine, nev, nh, nm, sk, inc2, inc3, inc4, gend)

test <- c(le[1], maine[1], nev[1], nh[1], nm[1], sk[1], inc2[1], inc3[1], inc4[1], gend[1])
x <- x[-1,]
y <- y[-1]



#*** State Prior Specifications
m_0 <- 1  #mu_0
m_maine <- -1  #mu_S1
m_nev <- 1  #mu_S2
m_nh <- -1  #mu_S3
m_nm <- -1  #mu_S4
m_sk <- -1  #mu_S5
m_wv <- 0  #mu_S6
m_q <- 0  #mu_Q
m_q2 <- -1  #mu_Q
m_q3 <- -1  #mu_Q
m_q4 <- -1  #mu_Q
m_g <- 1  #mu_G 

# m_all<- c(m_maine, m_nev, m_nh, m_nm, m_sk, m_q, m_g)
m_all<- c(m_maine, m_nev, m_nh, m_nm, m_sk, m_q2, m_q3, m_q4, m_g)

g <- 11#9#
a <- 0.1
b <- 5

#*** Constant that we will want during the MCMC
n <- length(le)
p <- 5+3+1+1#5+1+1+1#  #number of model coefficients

#*** Provide MCMC specifications
I <- 125000
betaStore <- matrix(0,I,p)
phiStore <- matrix(0,I,1)
yPredStore <- matrix(0,I,1)
# colnames(betaStore) <- c("beta0","betaMaine","beta_nev","beta_nh","beta_nm","beta_sk",
#                          "betaQ","betaG")
colnames(betaStore) <- c("beta0","betaMaine","beta_nev","beta_nh","beta_nm","beta_sk",
                         "betaQ2","betaQ3","betaQ4","betaG")
colnames(phiStore) <- "Phi"

#Part 4 - Model Validation
#(b) Create another storage matrix
#yPredStore <- matrix(0,I,1)

#*** Provide starting values
betaCur_0 <- m_0
betaCur_maine <- m_maine
betaCur_nev <- m_nev
betaCur_nh <- m_nh
betaCur_nm <- m_nm
betaCur_sk <- m_sk
betaCur_wv <- m_wv
betaCur_q <- m_q
betaCur_q2 <- m_q2
betaCur_q3 <- m_q3
betaCur_q4 <- m_q4
betaCur_g <- m_g
phiCur <- a/b

# betaCur<- c(betaCur_maine, betaCur_nev, betaCur_nh, betaCur_nm, 
#             betaCur_sk, betaCur_q, betaCur_g)
betaCur<- c(betaCur_maine, betaCur_nev, betaCur_nh, betaCur_nm,
                betaCur_sk, betaCur_q2, betaCur_q3, betaCur_q4, betaCur_g)

#*** Implement Gibbs using a for-loop
for (it in 1:I)
{
  #Draw Beta_0
  mnStar_0 <- ((sum(y-betaCur[1]*x[,1]-betaCur[2]*x[,2]-betaCur[3]*x[,3]-
                      betaCur[4]*x[,4]-betaCur[5]*x[,5]-betaCur[6]*x[,6]-
                      betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                 (m_0/g)) / (n+(1/g))
  sdStar_0 <- sqrt(1/(phiCur*(n+(1/g))))
  betaCur_0 <- rnorm(1,mnStar_0,sdStar_0)

  #Draw Beta_maine
  mnStar_maine <- (sum(x[,1]*(y-betaCur_0-betaCur[2]*x[,2]-betaCur[3]*x[,3]-
                                betaCur[4]*x[,4]-betaCur[5]*x[,5]-betaCur[6]*x[,6]-
                                betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                     (m_maine/g)) / (sum((x[,1])^2)+(1/g))
  sdStar_maine <- sqrt(1/(phiCur*((sum((x[,1])^2))+(1/g))))
  betaCur_maine <- rnorm(1,mnStar_maine,sdStar_maine)
  betaCur[1]<-betaCur_maine
  
  #Draw Beta_nev
  mnStar_nev <- (sum(x[,2]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[3]*x[,3]-
                              betaCur[4]*x[,4]-betaCur[5]*x[,5]-betaCur[6]*x[,6]-
                              betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                   (m_nev/g)) / (sum((x[,2])^2)+(1/g))
  sdStar_nev <- sqrt(1/(phiCur*((sum((x[,2])^2))+(1/g))))
  betaCur_nev <- rnorm(1,mnStar_nev,sdStar_nev)
  betaCur[2]<-betaCur_nev
  
  #Draw Beta_nh
  mnStar_nh <- (sum(x[,3]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                             betaCur[4]*x[,4]-betaCur[5]*x[,5]-betaCur[6]*x[,6]-
                             betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                  (m_nh/g)) / (sum((x[,3])^2)+(1/g))
  sdStar_nh <- sqrt(1/(phiCur*((sum((x[,3])^2))+(1/g))))
  betaCur_nh <- rnorm(1,mnStar_nh,sdStar_nh)
  betaCur[3]<-betaCur_nh
  
  #Draw Beta_nm
  mnStar_nm <- (sum(x[,4]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                             betaCur[3]*x[,3]-betaCur[5]*x[,5]-betaCur[6]*x[,6]-
                             betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                  (m_nm/g)) / (sum((x[,4])^2)+(1/g))
  sdStar_nm <- sqrt(1/(phiCur*((sum((x[,4])^2))+(1/g))))
  betaCur_nm <- rnorm(1,mnStar_nm,sdStar_nm)
  betaCur[4]<-betaCur_nm 
    
  #Draw Beta_sk
  mnStar_sk <- (sum(x[,5]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                             betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[6]*x[,6]-
                             betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                  (m_sk/g)) / (sum((x[,5])^2)+(1/g))
  sdStar_sk <- sqrt(1/(phiCur*((sum((x[,5])^2))+(1/g))))
  betaCur_sk <- rnorm(1,mnStar_sk,sdStar_sk)
  betaCur[5]<-betaCur_sk
    
  #Draw Beta_wv
  # mnStar_wv <- (sum(x[,6]*(y-betaCur_0-betaCur[-6]*x[,-6]))+(m_wv/g))/(sum((x[,6])^2)+(1/g))
  # sdStar_wv <- sqrt(1/(phiCur*((sum((x[,6])^2))+(1/g))))
  # betaCur_wv <- rnorm(1,mnStar_wv,sdStar_wv)
  # betaCur[6]<-betaCur_wv
  # betaCur_wv <- 0
  # betaCur[6] <- 0
    
  # #Draw Beta_q
  # mnStar_q <- (sum(x[,6]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
  #                           betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
  #                           betaCur[7]*x[,7]))+(m_q/g)) / (sum((x[,6])^2)+(1/g))
  # sdStar_q <- sqrt(1/(phiCur*((sum((x[,6])^2))+(1/g))))
  # betaCur_q <- rnorm(1,mnStar_q,sdStar_q)
  # betaCur[6]<-betaCur_q
  # 
  # #Draw Beta_g
  # mnStar_g <- (sum(x[,7]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
  #                           betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
  #                           betaCur[6]*x[,6]))+ (m_g/g)) / (sum((x[,7])^2)+(1/g))
  # sdStar_g <- sqrt(1/(phiCur*((sum((x[,7])^2))+(1/g))))
  # betaCur_g <- rnorm(1,mnStar_g,sdStar_g)
  # betaCur[7]<-betaCur_g
  
  #Draw Beta_q2
  mnStar_q <- (sum(x[,6]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                            betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
                            betaCur[7]*x[,7]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                 (m_q/g)) / (sum((x[,6])^2)+(1/g))
  sdStar_q <- sqrt(1/(phiCur*((sum((x[,6])^2))+(1/g))))
  betaCur_q <- rnorm(1,mnStar_q,sdStar_q)
  betaCur[6]<-betaCur_q

  #Draw Beta_q3
  mnStar_q <- (sum(x[,7]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                            betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
                            betaCur[6]*x[,6]-betaCur[8]*x[,8]-betaCur[9]*x[,9]))+
                 (m_q/g)) / (sum((x[,7])^2)+(1/g))
  sdStar_q <- sqrt(1/(phiCur*((sum((x[,7])^2))+(1/g))))
  betaCur_q <- rnorm(1,mnStar_q,sdStar_q)
  betaCur[7]<-betaCur_q

  #Draw Beta_q4
  mnStar_q <- (sum(x[,8]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                            betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
                            betaCur[6]*x[,6]-betaCur[7]*x[,7]-betaCur[9]*x[,9]))+
                 (m_q/g)) / (sum((x[,8])^2)+(1/g))
  sdStar_q <- sqrt(1/(phiCur*((sum((x[,8])^2))+(1/g))))
  betaCur_q <- rnorm(1,mnStar_q,sdStar_q)
  betaCur[8]<-betaCur_q

  #Draw Beta_g
  mnStar_g <- (sum(x[,9]*(y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                            betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
                            betaCur[6]*x[,6]-betaCur[7]*x[,7]-betaCur[8]*x[,8]))+
                 (m_g/g)) / (sum((x[,9])^2)+(1/g))
  sdStar_g <- sqrt(1/(phiCur*((sum((x[,9])^2))+(1/g))))
  betaCur_g <- rnorm(1,mnStar_g,sdStar_g)
  betaCur[9]<-betaCur_g

  #Draw phi
  aStar <- ((n+p)/2)+a

  bTemp1 <- .5*sum( (y-betaCur_0-betaCur[1]*x[,1]-betaCur[2]*x[,2]-
                       betaCur[3]*x[,3]-betaCur[4]*x[,4]-betaCur[5]*x[,5]-
                       betaCur[6]*x[,6]-betaCur[7]*x[,7]-betaCur[8]*x[,8]-
                       betaCur[9]*x[,9])^2)
  bTemp2 <- .5*(1/g)*( (betaCur_0-m_0)^2 + sum((betaCur-m_all)^2))
  bStar <- bTemp1+bTemp2+b

  phiCur <- rgamma(1,aStar,bStar)
  
  #Update Values
  # betaCur<- c(betaCur_maine, betaCur_nev, betaCur_nh, betaCur_nm,
  #                 betaCur_sk, betaCur_q, betaCur_g)
  # betaCur<- c(betaCur_maine, betaCur_nev, betaCur_nh, betaCur_nm,
  #             betaCur_sk, betaCur_q2, betaCur_q3, betaCur_q4, betaCur_g)

  #Store the draws
  betaStore[it,1] <- betaCur_0
  betaStore[it,2:(1+length(betaCur))] <- betaCur
  phiStore[it,1] <- phiCur
  
  #Part 4 - Model Validation
  #(c) Simulate a prediction from your model
  #predY
  muPred <- betaCur_0+betaCur[1]*test[1]+betaCur[2]*test[2]+betaCur[3]*test[3]+betaCur[4]*test[4]+
    +betaCur[5]*test[5]+betaCur[6]*test[6]+betaCur[7]*test[7]+betaCur[8]*test[8]+betaCur[9]*test[9]
  sdPred <- sqrt(1/phiCur)
  yPredStore[it,1] <- rnorm(1, muPred, sdPred)
}


#Part 2 - Assess Convergence
#* Trace plots
par(mfrow=c(3, 3))
plot(betaStore[,1], main="Beta_0", type="l")
plot(betaStore[,2], main="Beta_maine", type="l")
plot(betaStore[,3], main="Beta_nev", type="l")
plot(betaStore[,4], main="Beta_nh", type="l")
plot(betaStore[,5], main="Beta_nm", type="l")
plot(betaStore[,6], main="Beta_sk", type="l")
# plot(betaStore[,7], main="Beta_wv", type="l")
# plot(betaStore[,7], main="Beta_q", type="l")
# plot(betaStore[,8], main="Beta_g", type="l")
plot(betaStore[,7], main="Beta_q2", type="l")
plot(betaStore[,8], main="Beta_q3", type="l")
plot(betaStore[,9], main="Beta_q4", type="l")
plot(betaStore[,10], main="Beta_g", type="l")
plot(phiStore, main="Phi", type="l")

#remove Burn-in
BI <- I - 10000
noBiBeta <- betaStore[-c(1:BI),]
noBiPhi <- phiStore[-c(1:BI),]
noBiPred <- yPredStore[-c(1:BI),]

par(mfrow=c(4,3))
plot(noBiBeta[,1], main="Beta_0", type="l")
plot(noBiBeta[,2], main="Beta_maine", type="l")
plot(noBiBeta[,3], main="Beta_nev", type="l")
plot(noBiBeta[,4], main="Beta_nh", type="l")
plot(noBiBeta[,5], main="Beta_nm", type="l")
plot(noBiBeta[,6], main="Beta_sk", type="l")
# plot(noBiBeta[,7], main="Beta_wv", type="l")
# plot(noBiBeta[,7], main="Beta_q", type="l")
# plot(noBiBeta[,8], main="Beta_g", type="l")
plot(noBiBeta[,7], main="Beta_q2", type="l")
plot(noBiBeta[,8], main="Beta_q3", type="l")
plot(noBiBeta[,9], main="Beta_q4", type="l")
plot(noBiBeta[,10], main="Beta_g", type="l")
# 
# par(mfrow=c(1,1))
plot(noBiPhi, main="Phi", type="l")


# Convergence Tests
convTest <- function(vec, beta)
{
  test <- geweke.diag(as.mcmc(vec), frac1=0.1, frac2=0.5)
  pVal <- 1-pnorm(abs(test$z), 0, 1)
  print(pVal)
  print(pVal < 0.1)
  if(pVal < 0.1)
  {
    warning(c("Geweke failed for beta ", beta))
  }
  test <- ks.test(vec[1:5000], vec[5001:10000])
  if(test$p.value < 0.05)
  {
    warning(c("KS failed for beta ", beta))
  }
}

for(beta in 1:length(betaStore[1,]))
{
  if(beta != 7)
  {
    convTest(noBiBeta[,beta], beta)
  }
}
convTest(noBiPhi, 10)
#Raw data > pVal= 0.4995352
#scaled data > pVal = 0.318735


#Part 3 - Make Inference

#1. Plot the histograms of each distribution

# par(mfrow=c(3,3))
# hist(betaStore[,1], main="Beta_0")
# abline(v=c(mo0,hpd0), col="red")
# hist(betaStore[,2], main="Beta_maine")
# hist(betaStore[,3], main="Beta_nev")
# hist(betaStore[,4], main="Beta_nh")
# hist(betaStore[,5], main="Beta_nm")
# hist(betaStore[,6], main="Beta_sk")
# hist(betaStore[,7], main="Beta_wv")
# hist(betaStore[,8], main="Beta_q")
# hist(betaStore[,9], main="Beta_g")
# 
# par(mfrow=c(3,3))
# hist(betaStore[,1], main="Beta_0", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,2], main="Beta_maine", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,3], main="Beta_nev", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,4], main="Beta_nh", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,5], main="Beta_nm", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,6], main="Beta_sk", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,7], main="Beta_wv", breaks=20, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,8], main="Beta_q", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# hist(betaStore[,9], main="Beta_g", breaks=15, xlim=c(-2,2), col="cadetblue",border="white")
# 

#3. MAP and 95% HPD intervals
#mode
getMode <- function(vec)
{
  d <- density(vec, bw = "nrd0")
  #plot(d)
  mode <- d$x[d$y==max(d$y)]
  if (length(mode)>1){mode <- mean(mode)}
  return(mode)
}

mo_0 <- getMode(noBiBeta[,1])
mo_maine<- getMode(noBiBeta[,2])
mo_nev <- getMode(noBiBeta[,3])
mo_nh <- getMode(noBiBeta[,4])
mo_nm <- getMode(noBiBeta[,5])
mo_sk<- getMode(noBiBeta[,6])
# mo_wv <- getMode(noBiBeta[,7])
# mo_q <- getMode(noBiBeta[,7])
# mo_g <- getMode(noBiBeta[,8])
mo_q2 <- getMode(noBiBeta[,7])
mo_q3 <- getMode(noBiBeta[,8])
mo_q4 <- getMode(noBiBeta[,9])
mo_g <- getMode(noBiBeta[,10])
mo_phi <- getMode(noBiPhi)
moPred <- getMode(noBiPred)
#* HPD
hpd_0 <- HPDinterval(as.mcmc(noBiBeta[,1]),.95)
hpd_maine<- HPDinterval(as.mcmc(noBiBeta[,2]),.95)
hpd_nev <- HPDinterval(as.mcmc(noBiBeta[,3]),.95)
hpd_nh <- HPDinterval(as.mcmc(noBiBeta[,4]),.95)
hpd_nm <- HPDinterval(as.mcmc(noBiBeta[,5]),.95)
hpd_sk<- HPDinterval(as.mcmc(noBiBeta[,6]),.95)
# hpd_wv <- HPDinterval(as.mcmc(noBiBeta[,7]),.95)
# hpd_q <- HPDinterval(as.mcmc(noBiBeta[,7]),.95)
# hpd_g <- HPDinterval(as.mcmc(noBiBeta[,8]),.95)
hpd_q2 <- HPDinterval(as.mcmc(noBiBeta[,7]),.95)
hpd_q3 <- HPDinterval(as.mcmc(noBiBeta[,8]),.95)
hpd_q4 <- HPDinterval(as.mcmc(noBiBeta[,9]),.95)
hpd_g <- HPDinterval(as.mcmc(noBiBeta[,10]),.95)
hpd_phi <- HPDinterval(as.mcmc(noBiPhi),.95)
hpdPred <- HPDinterval(as.mcmc(noBiPred),.95)


par(mfrow=c(3,2))
hist(noBiBeta[,2], main="Beta_maine", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_maine,hpd_maine), col="red")
hist(noBiBeta[,3], main="Beta_nev", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_nev,hpd_nev), col="red")
hist(noBiBeta[,4], main="Beta_nh", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_nh,hpd_nh), col="red")
hist(noBiBeta[,5], main="Beta_nm", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_nm,hpd_nm), col="red")
hist(noBiBeta[,6], main="Beta_sd", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_sk,hpd_sk), col="red")
# hist(noBiBeta[,7], main="Beta_wv", breaks=20, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
# abline(v=c(mo_wv,hpd_wv), col="red")

par(mfrow=c(3,2))
hist(noBiBeta[,1], main="Beta_0", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_0,hpd_0), col="red")
# hist(noBiBeta[,7], main="Beta_q", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
# abline(v=c(mo_q,hpd_q), col="red")
# hist(noBiBeta[,8], main="Beta_g", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
# abline(v=c(mo_g,hpd_g), col="red")
hist(noBiBeta[,7], main="Beta_q2", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_q2,hpd_q2), col="red")
hist(noBiBeta[,8], main="Beta_q3", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_q3,hpd_q3), col="red")
hist(noBiBeta[,9], main="Beta_q4", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_q4,hpd_q4), col="red")
hist(noBiBeta[,10], main="Beta_g", breaks=15, xlim=c(-2,2), col="cadetblue",border="white",probability=0, xlab="Beta Value")
abline(v=c(mo_g,hpd_g), col="red")
hist(noBiPhi, main="Phi", breaks=15, col="cadetblue", border="white", probability=1, xlab="Phi Value")
abline(v=c(mo_phi,hpd_phi), col="red")
hist(noBiPred, main="Pred", prob=TRUE)
abline(v=c(moPred,hpdPred), col="red")

par(mfrow=c(1,1))
hist(noBiPred, main="Pred", prob=TRUE)
abline(v=c(exPred,ciPred), col="red")


#Residual Analysis
# y_est <- mo_0+mo_maine*maine+mo_nev*nev+mo_nh*nh+mo_nm*nm+mo_sk*sk+
#   mo_q*inc+mo_g*gend
y_est <- mo_0+mo_maine*maine+mo_nev*nev+mo_nh*nh+mo_nm*nm+mo_sk*sk+
  mo_q2*inc2+mo_q3*inc3+mo_q4*inc4+mo_g*gend
res <- y_est - y
par(mfrow=c(2,2))
plot(y_est, res)
qqnorm(res)
hist(res, breaks="FD")
print(shapiro.test(res))
print(ks.test(res, "pnorm", mean(res), sd(res)))

# Classical Regression
classical_reg <- lm(le ~ gend+factor(inc)+maine+nev+nh+nm+sk)
print(summary(classical_reg))
layout(matrix(1:4, c(2,2)))
plot(classical_reg)

par(mfrow=c(1,1))
hist(classical_reg$residuals)
# plot(classical_reg$residuals)
# qqnorm(classical_reg$residuals)

print(shapiro.test(classical_reg$residuals))
print(ks.test(classical_reg$residuals, "pnorm", mean(classical_reg$residuals),
              sd(classical_reg$residuals)))
