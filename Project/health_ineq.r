rm(list=ls()) #clear workspace
library(coda) #MCMC package (?)

#*** Get the Data
dat <- read.table("health_ineq_online_table_5_Mod2.csv", sep=",", header=TRUE)

#** Organize the data
# States are as follows:
# Maine, Nevada, New Hampshire, New Mexico, South Dakota, West Virginia
le <- dat[,5]
maine <- c(rep(1,14), rep(0,70))
nev <- c(rep(0,14), rep(1,14), rep(0,56))
nh <- c(rep(0,28), rep(1,14), rep(0,42))
nm <- c(rep(0,42), rep(1,14), rep(0,28))
sk <- c(rep(0,56), rep(1,14), rep(0,14))
wv <- c(rep(0,84))
# wv <- c(rep(0,70), rep(1,14)) # commented out because WV is the "baseline"
for(col in 6:12)
{
  le <- c(le, dat[,col])
  maine <- c(maine, rep(1,14), rep(0,70))
  nev <- c(nev, rep(0,14), rep(1,14), rep(0,56))
  nh <- c(nh, rep(0,28), rep(1,14), rep(0,42))
  nm <- c(nm, rep(0,42), rep(1,14), rep(0,28))
  sk <- c(sk, rep(0,56), rep(1,14), rep(0,14))
  wv <- c(wv, rep(0,84))
  # wv <- c(wv, rep(0,70), rep(1,14))
}
inc <- c(rep(1,6*14), rep(2,6*14), rep(3,6*14), rep(4,6*14))
inc <- c(inc, inc)
gend <- c(rep(0,14*6*4), rep(1,14*6*4))

le <- scale(le)
maine <- scale(maine)
nev <- scale(nev)
nh <- scale(nh)
nm <- scale(nm)
sk <- scale(sk)
# wv <- scale(wv)
# inc <- log(inc)
inc <- scale(inc)
gend <- scale(gend)

# plot(gend, log(le))
# plot(sqrt(inc), log(le))

reg <- lm(le ~ gend+inc)
reg2 <- lm(le ~ gend+inc+maine+nev+nh+nm+sk)
print(summary(reg2))

layout(matrix(1:4,2,2))
plot(reg2)

# reg_inc <- lm(le ~ log(inc))
# plot(reg_inc)

layout(c(1,1))
hist(le)
# hist(log(le))

qqnorm(le)
lines(seq(-3,3,length.out=length(le)), seq(min(le),max(le),length.out=length(le)))
# qqnorm(log(le))
# lines(seq(-3,3,length.out=length(le)), seq(min(log(le)),max(log(le)),length.out=length(le)))

# sd(log(le))

par(mfrow=c(1,1))
hist(reg2$residuals)
plot(reg2$residuals)
qqnorm(reg2$residuals)

print(shapiro.test(reg2$residuals))
print(ks.test(reg2$residuals,pnorm(length(reg2$residuals),mean(reg2$residuals),sd(reg2$residuals))))