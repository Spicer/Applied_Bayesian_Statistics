# require(tcltk)
# library(tkrplot)

# ## function to display plot, called by tkrplot and embedded in a window
# plotIt<-function(){ plot(x=1:10, y=1:10) }
# ## create top level window
# tt<-tktoplevel()
# ## variable to wait on like a condition variable, to be set by event handler
# done <- tclVar(0)
# ## bind to the window destroy event, set done variable when destroyed
# tkbind(tt,"<Destroy>",function() tclvalue(done) <- 1)
# ## Have tkrplot embed the plot window, then realize it with tkgrid
# tkgrid(tkrplot(tt,plotIt))
# ## wait until done is true
# tkwait.variable(done)

plot(1:10,(1:10)/2)
plot(1:10,1:10)

# library(PythonInR)

# pyExec('
# 	import matplotlib.pyplot as plt
# 	plt.plot([0,1],[1,1]
# 	plt.show')
# code <-
# BEGIN.Python()
# import os
# os.getcwd()
# dir(os)
# x = 3**3
# for i in range(10):
# 	if (i > 5):
# 		print(i)
# END.Python
# x=1:10
# y=1:10
# system('python - c "import matplotlib.pyplot as plt
# 	plt.plot(x,y)
# 	plt.show()
# 	exit()"
# 	')