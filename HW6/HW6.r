rm(list=ls()) #clear workspace

data = read.table("carDist.txt", header = TRUE, sep = ",")
dist_frame = data[-1]
speed_frame = data[-2]

speed = speed_frame[,1]
dist = dist_frame[,1]

model <- lm(dist ~ speed, data=data)
summary(model)

model2 <- lm(dist ~ poly(speed, 2, raw=TRUE), data=data)
summary(model2)

res2 = model2["residuals"][[1]]

# model3 <- lm(dist ~ poly(speed, 3, raw=TRUE), data=data)
# summary(model3)

dist_est = -17.579095 + 3.932409*speed
dist_est2 = 2.47014 + 0.91329*speed + 0.09996*speed^2
# dist_est3 = -19.50505 + 6.80111*x - 0.34966*x^2 + 0.01025*x^3

plot(speed,dist)
title("Regression Results")
par(new=T)
lines(speed,dist_est)

plot(speed,dist)
title("Regression Results")
par(new=T)
lines(speed,dist_est2)

par(mfrow=c(1,1))
plot(speed, res2, ylab="residuals")
lines(speed,rep(0, length(speed)))
qqnorm(res2)
print(mean(res2))


# plot(speed,dist)
# title("Regression Results")
# par(new=T)
# lines(x,y_est3)

par(mfrow=c(2,1))
# hist(res)
hist(res2)

# shapiro.test(res)
print(shapiro.test(res2))

ks.test(res2, pnorm)
# ks.test(res, pnorm(length(res),mean(res),sd(res)))
ks.test(res2,pnorm(length(res2),mean(res2),sd(res2)))
