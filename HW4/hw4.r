


## Number 2
alpha = 100
beta = 1
dat = read.table("strikes.txt", header = FALSE, sep = "\n")
# dat=dat[,-1]
num_data = length(dat[,1])
sum_data = sum(dat[,1])

d1 <- alpha + sum_data
d2 = beta + num_data
expected_value = d1/d2
variance = d1/(d2^2)
cred_int = qgamma(c(0.025,0.975),d1,rate=d2)

#part b:
num_samples = 10000
posterior.sample <- rgamma(num_samples,d1,d2)
av = mean(posterior.sample)
s_var = var(posterior.sample)
s_cred_int = quantile(posterior.sample, c(0.025,0.975))

lambda_vec = seq(0,250, by=0.1)
prior = dgamma(lambda_vec, alpha, rate=beta)
post = dgamma(lambda_vec, d1, rate=d2)

plot(lambda_vec, post,'l', xlab="", ylab="", main="", col='green')
title('Number of Strikes', xlab = 'strikes', ylab = 'density')
par(new=T)
lines(lambda_vec, prior, 'l', xlab="", ylab="", main="", col='red')#, axes=FALSE)
par(new=T)
jo = hist(posterior.sample, plot=FALSE, xlab="", ylab="", main="")#, add=T)#axes=FALSE)
jo[[1]][1:length(jo[[1]])-1]=(jo[[1]][1:length(jo[[1]])-1] + jo[[1]][2:length(jo[[1]])]) / 2
lines(jo[[1]], c(jo[[2]]/num_samples,0), 'h', xlab="", ylab="", main="")
mtext("posterior.sample", side=4, line=2)
legend('topright', c('prior', 'posterior'), pch=1, col=c('red', 'green'))



## Number 3
x = seq(0,1,by=0.01)
density=dbeta(x, 1, 1)
plot(x,density, type='l')


## Number 4
x_vec = seq(0,100,by=1)
# plot(x_vec, dnorm(x_vec, 70, 5, log=F))
# likelihood <- function(mu, sigma, x, n)
# {
#   return (2*pi*sigma^2)^(-n/2)*exp((-.5/(sigma^2))*sum( (y-mu)^2))
# }
posterior <- function(phi,m,q,dat,n)
{
  m_star <- (phi*sum(dat)+q*m)/(n*phi+q)
  p_star <- n*phi+q
  mu_vec <- seq(0, 100, by=0.1)
  post <- dnorm(mu_vec, m_star, p_star)
  plot(mu_vec, post, type="l", xlab="x", ylab="Posterior Density")
  print("The posterior expectation is:")
  print(m_star)
  print("The posterior standard deviation is:")
  print(sqrt(1/p_star))
}
posterior(0.01, 70, 0.04, rep(75,30), 30)
