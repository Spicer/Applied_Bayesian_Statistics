import numpy as np
import scipy
import scipy.io as sio
import matplotlib.pyplot as plt

def scaleData(data):
    mean = np.mean(data, axis=0)
    std = np.std(data, axis=0)
    data = (data-mean)/std

    return data, mean, std

def gibbsSampler(y_vec, x_mat, test_data, m_vec, g_val, a_val, b_val, it):
    num_samples = y_vec.shape[0]
    num_coeffs = x_mat.shape[1]

    beta_cur = m_vec
    phi_cur = a_val/b_val
    beta_store = np.empty((iters,num_coeffs))
    phi_store = np.empty((iters,1))
    y_pred_store = np.empty((iters,1))
    y_star = np.empty((num_samples,num_coeffs))
    m_star = m_vec
    sd_star = m_vec

    #*** Implement Gibbs using a for-loop
    for it in range(iters):
        for param in range(num_coeffs):
            #Draw Beta_0
            # print(np.array([np.delete(beta_cur,param)]).shape)
            # print(np.delete(x_mat,param,axis=1).shape)
            # print((np.array([np.delete(beta_cur,param)])*np.delete(x_mat,param,axis=1)).shape)
            y_star[:,param] = y_vec - np.delete(beta_cur,param) @ np.delete(x_mat,param, axis=1).T
            check = y_star[:,param] - (y_vec-beta_cur[1]*x_mat[:,1]-beta_cur[2]*x_mat[:,2]-beta_cur[3]*x_mat[:,3])
            # print(np.where(check>0.01))
            # print(check.shape)
            m_star[param] = (np.dot(x_mat[:,param],y_star[:,param]) + m_vec[param]/g_val) / (np.dot(x_mat[:,param],x_mat[:,param]) + 1/g_val)
            sd_star[param] = np.sqrt(1/(phi_cur*(np.dot(x_mat[:,param],x_mat[:,param]) + 1/g_val)))
            beta_cur[param] = np.random.normal(loc=m_star[param], scale=sd_star[param])
            # print(beta_cur[param])

        # print(beta_cur)
        #Draw phi
        a_star = num_samples/2+num_coeffs/2+a_val
        b_temp1 = 0.5*np.dot(y_vec-beta_cur @ x_mat.T, y_vec-beta_cur @ x_mat.T)
        b_temp2 = 0.5*(1/g_val)*np.dot(beta_cur-m_vec, beta_cur-m_vec)
        b_star = b_temp1+b_temp2+b_val
        phi_cur = np.random.gamma(a_star, scale=1/b_star)
        # print(a_star, b_star, phi_cur)

        #Store the draws
        beta_store[it,:] = beta_cur
        phi_store[it] = phi_cur

        #predY
        # print(it)
        mu_pred = np.dot(beta_cur,test_data)#betaCur0+betaCurZ*dat2[1,1]+betaCurW*dat2[1,2]+betaCurM*dat2[1,3]
        sd_pred = np.sqrt(1/phi_cur)
        y_pred_store[it] = np.random.normal(loc=mu_pred, scale=sd_pred)

    return np.append(beta_store, phi_store, axis=1), y_pred_store

data = np.loadtxt("HW7/airquality.csv", delimiter=",", skiprows=1)
print(data)

temp = data[:, 3]
x_mat = data[:,[0, 2, 4]]
print(x_mat)

# ozone = x_mat[:, 0]
# wind = x_mat[:, 1]
# month = x_mat[:, 2]

#*** Stat0.5 Prior Specifications
m_0 = 0.1  #mu_0
m_o = 0.5  #mu_Z
m_w = 0.15  #mu_W
m_m = 0.7  #mu_M
m_vec = np.array([m_0, m_o, m_w, m_m])

g_val = 2
a_val = 0.05
b_val = 0.05

#*** Constant that we will want during the MCMC
num_samples = temp.shape[0]
num_coeffs = x_mat.shape[1]  #number of model coefficients

#*** Provide MCMC specifications
iters = 50000
# betaStore = matrix(0,iters,p)
# phiStore = matrix(0,iters,1)
# yPredStore = matrix(0,iters,1)

#*** Provide starting values
# betaCur0 = m0
# betaCurZ = mZ
# betaCurW = mW
# betaCurM = mM
# phiCur = a_val/b_val

x_mat, pdfsj, dslkan = scaleData(x_mat)
temp, pdfsj, dslkan = scaleData(temp)
print(x_mat)
print(np.array([np.ones(num_samples)]).T.shape)
x_mat = np.append(np.array([np.ones(num_samples)]).T, x_mat, axis=1)
print(temp.shape, x_mat.shape)

test_data = x_mat[0,:]
test_temp = temp[0]
temp = np.delete(temp,0)
x_mat = np.delete(x_mat,0, axis=0)
gibbs, pred = gibbsSampler(temp, x_mat, test_data, m_vec, g_val, a_val, b_val, iters)

gibbs = np.delete(gibbs, np.arange(iters-10000), axis=0)
pred = np.delete(pred, np.arange(iters-10000))

print(np.mean(pred), np.std(pred), test_temp)
print(np.mean(gibbs, axis=0), np.std(gibbs, axis=0))