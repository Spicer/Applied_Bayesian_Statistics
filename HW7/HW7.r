rm(list=ls()) #clear workspace

library(coda) #MCMC package (?)


#*** Get the Data
dat <- read.table("airquality.csv", sep=",", header<-TRUE)
dat <- scale(dat)
dat2 <- dat[1, ,drop=FALSE]
dat <- dat[-1, ,drop=FALSE]
y <- dat[,"Temp"]
x <- dat[,c(1,3,5)]
x_z <- x[,1]
x_w <- x[,2]
x_m <- x[,3]


#*** Stat0.5 Prior Specifications
m0 <- 0.1  #mu_0
mZ <- 0.5  #mu_Z
mW <- 0.15  #mu_W
mM <- 0.7  #mu_M

g <- 2
a <- 0.05
b <- 0.05

#*** Constant that we will want during the MCMC
num_samples <- dim(dat)[[1]]
p <- 3+1  #number of model coefficients

#*** Provide MCMC specifications
I <- 50000
betaStore <- matrix(0,I,p)
phiStore <- matrix(0,I,1)
colnames(betaStore) <- c("beta0","betaZ","betaW","betaM")
colnames(phiStore) <- "Phi"
yPredStore <- matrix(0,I,1)

#*** Provide starting values
betaCur0 <- m0
betaCurZ <- mZ
betaCurW <- mW
betaCurM <- mM
phiCur <- 1


#*** Implement Gibbs using a for-loop
for (c in 1:I)
{
  #Draw Beta_0
  yi_star0 <- y - betaCurZ*x_z - betaCurW*x_w - betaCurM*x_m
  mnStar0 <- (sum(yi_star0) + m0/g) / (num_samples + g^(-1))
  sdStar0 <- sqrt((phiCur*(num_samples + g^(-1)))^(-1))
  betaCur0 <- rnorm(1,mnStar0,sdStar0)
  
  #Draw Beta_Z
  yi_starZ <- y - betaCur0 - betaCurW*x_w - betaCurM*x_m
  mnStarZ <- (sum(yi_starZ*x_z) + mZ/g) / (sum(x_z ^ 2) + g^(-1))
  sdStarZ <- sqrt((phiCur*(sum(x_z ^ 2) + g^(-1)))^(-1))
  betaCurZ <- rnorm(1,mnStarZ,sdStarZ)
  
  #Draw Beta_W
  yi_starW <- y - betaCur0 - betaCurZ*x_z - betaCurM*x_m
  mnStarW <- (sum(yi_starW*x_w) + mW/g) / (sum(x_w ^ 2) + g^(-1))
  sdStarW <- sqrt((phiCur*(sum(x_w ^ 2) + g^(-1)))^(-1))
  betaCurW <- rnorm(1,mnStarW,sdStarW)
  
  #Draw Beta_M
  yi_starM <- y - betaCur0 - betaCurZ*x_z - betaCurW*x_w
  mnStarM <- (sum(yi_starM*x_m) + mM/g) / (sum(x_m ^ 2) + g^(-1))
  sdStarM <- sqrt((phiCur*(sum(x_m ^ 2) + g^(-1)))^(-1))
  betaCurM <- rnorm(1,mnStarM,sdStarM)
  
  
  #Draw phi
  aStar <- num_samples/2+p/2+a
  
  bTemp1 <- .5*sum( (y-betaCur0-betaCurZ*x[,1]-betaCurW*x[,2]-betaCurM*x[,3])^2)
  bTemp2 <- .5*(1/g)*( (betaCur0-m0)^2+(betaCurZ-mZ)^2+(betaCurW-mW)^2+(betaCurM-mM)^2)
  bStar <- bTemp1+bTemp2+b
  
  phiCur <- rgamma(1,aStar,bStar)
  
  #Store the draws
  betaStore[c,1] <- betaCur0
  betaStore[c,2] <- betaCurZ
  betaStore[c,3] <- betaCurW
  betaStore[c,4] <- betaCurM
  phiStore[c,1] <- phiCur
  
  #predY
  muPred <- betaCur0+betaCurZ*dat2[1,1]+betaCurW*dat2[1,2]+betaCurM*dat2[1,3]
  sdPred <- sqrt(1/phiCur)
  yPredStore[c,1] <- rnorm(1, muPred, sdPred)
}


#* Trace plots
par(mfrow=c(3,2))
plot(betaStore[,1], main="Beta0", type="l")
plot(betaStore[,2], main="BetaZ", type="l")
plot(betaStore[,3], main="BetaW", type="l")
plot(betaStore[,4], main="BetaM", type="l")
plot(phiStore[,1], main="Phi", type="l")
#remove Burn-in
BI <- 25000
noBiBeta <- betaStore[-c(1:BI),]
noBiPhi <- phiStore[-c(1:BI),]
noBiPred <- yPredStore[-c(1:BI),]

par(mfrow=c(3,2))
plot(noBiBeta[,1], main="Beta0", type="l")
plot(noBiBeta[,2], main="BetaZ", type="l")
plot(noBiBeta[,3], main="BetaW", type="l")
plot(noBiBeta[,4], main="BetaM", type="l")
plot(noBiPhi, main="Phi", type="l")
plot(noBiPred, main="Prediction", type="l")

gewekeTest <- function(vec)
{
  test <- geweke.diag(as.mcmc(vec), frac1=0.1, frac2=0.5)
  pVal <- 1-pnorm(abs(test$z), 0, 1)
  alpha <- 0.05
  if(pVal < alpha/2)
  {
    warning("MCMC not converged!")
  }
  print(pVal)
}

for (beta in 1:length(betaStore[1,]))
{
  gewekeTest(noBiBeta[,beta])
}
gewekeTest(noBiPhi)


par(mfrow=c(3,2))
hist(noBiBeta[,1], main="Beta0", prob=TRUE)
hist(noBiBeta[,2], main="BetaZ", prob=TRUE)
hist(noBiBeta[,3], main="BetaW", prob=TRUE)
hist(noBiBeta[,4], main="BetaM", prob=TRUE)
hist(noBiPhi, main="Phi", prob=TRUE)
hist(noBiPred, main="Pred", prob=TRUE)

ex0 <- mean(noBiBeta[,1])
exZ <- mean(noBiBeta[,2])
exW <- mean(noBiBeta[,3])
exM <- mean(noBiBeta[,4])
exPh <- mean(noBiPhi)
exPred <- mean(noBiPred)

sd0 <- sd(noBiBeta[,1])
sdZ <- sd(noBiBeta[,2])
sdW <- sd(noBiBeta[,3])
sdM <- sd(noBiBeta[,4])
sdPh <- sd(noBiPhi)
sdPred <- sd(noBiPred)

ci0 <- quantile(noBiBeta[,1], prob=c(.025, .975))
ciZ <- quantile(noBiBeta[,2], prob=c(.025, .975))
ciW <- quantile(noBiBeta[,3], prob=c(.025, .975))
ciM <- quantile(noBiBeta[,4], prob=c(.025, .975))
ciPh <- quantile(noBiPhi, prob=c(.025, .975))
ciPred <- quantile(noBiPred, prob=c(.025, .975))

#overlay mean and quantiles of histograms
par(mfrow=c(3,2))
hist(noBiBeta[,1], main="Beta0", prob=TRUE)
abline(v=c(ex0,ci0), col="red")
hist(noBiBeta[,2], main="BetaZ", prob=TRUE)
abline(v=c(exZ,ciZ), col="red")
hist(noBiBeta[,3], main="BetaW", prob=TRUE)
abline(v=c(exW,ciW), col="red")
hist(noBiBeta[,4], main="BetaM", prob=TRUE)
abline(v=c(exM,ciM), col="red")
hist(noBiPhi, main="Phi", prob=TRUE)
abline(v=c(exPh,ciPh), col="red")

par(mfrow=c(1,1))
hist(noBiPred, main="Pred", prob=TRUE)
abline(v=c(exPred,ciPred), col="red")

#mode
getMode <- function(vec)
{
  d <- density(vec, bw = "sj")
  #plot(d)
  mode <- d$x[d$y==max(d$y)]
  if (length(mode)>1)
  {
    mode <- mean(mode)
  }
  return(mode)
}
mo0 <- getMode(noBiBeta[,1])
moZ <- getMode(noBiBeta[,2])
moW <- getMode(noBiBeta[,3])
moM <- getMode(noBiBeta[,4])
moPh <- getMode(noBiPhi)
moPred <- getMode(noBiPred)

#* HPD
hpd0 <- HPDinterval(as.mcmc(noBiBeta[,1]),.95)
hpdZ <- HPDinterval(as.mcmc(noBiBeta[,2]),.95)
hpdW <- HPDinterval(as.mcmc(noBiBeta[,3]),.95)
hpdM <- HPDinterval(as.mcmc(noBiBeta[,4]),.95)
hpdPh <- HPDinterval(as.mcmc(noBiPhi),.95)
hpdPred <- HPDinterval(as.mcmc(noBiPred),.95)

par(mfrow=c(3,2))
hist(noBiBeta[,1], main="Beta0", prob=TRUE)
abline(v=c(mo0,hpd0), col="red")
hist(noBiBeta[,2], main="BetaZ", prob=TRUE)
abline(v=c(moZ,hpdZ), col="red")
hist(noBiBeta[,3], main="BetaW", prob=TRUE)
abline(v=c(moW,hpdW), col="red")
hist(noBiBeta[,4], main="BetaM", prob=TRUE)
abline(v=c(moM,hpdM), col="red")
hist(noBiPhi, main="Phi", prob=TRUE)
abline(v=c(moPh,hpdPh), col="red")
hist(noBiPred, main="Pred", prob=TRUE)
abline(v=c(moPred,hpdPred), col="red")

pr0 <- sum(noBiBeta[,1]>0)/(I-BI)
prZ <- sum(noBiBeta[,2]>0)/(I-BI)
prW <- sum(noBiBeta[,3]>0)/(I-BI)
prM <- sum(noBiBeta[,4]>0)/(I-BI)