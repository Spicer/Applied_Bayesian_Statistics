rm(list=ls())

#*** Get an R MCMC package
#If you do not have the coda package, install it:
#install.packages("coda", repo="http://R.research.att.com")

library(coda)


#*** Get the Data
dat <- read.table( "airquality.csv", sep=",", header=TRUE)
y <- dat$Temp
x <- dat[,c(1,3,5)]


#*** State Prior Specifications
m0 <- XXX  #mu_0 
mZ <- XXX  #mu_Z 
mW <- XXX  #mu_W 
mM <- XXX  #mu_M 

g <- XXX
a <- XXX
b <- XXX

#*** Constant that we will want during the MCMC
n <- dim(dat)[[1]]
p <- 3+1  #number of model coefficients

#*** Provide MCMC specifications
I <- 10000
betaStore <- matrix(0,I,p)
phiStore <- matrix(0,I,1)
colnames(betaStore) <- c("beta0","betaZ","betaW","betaM")
colnames(phiStore) <- "Phi"

#*** Provide starting values
betaCur0 <- m0
betaCurZ <- mZ
betaCurW <- mW
betaCurM <- mM
phiCur <- 1 

  
#*** Implement Gibbs using a for-loop
for (c in 1:I){

  #Draw Beta_0
  mnStar0 <- XXX
  sdStar0 <- XXX
  betaCur0 <- rnorm(1,mnStar0,sdStar0)

  #Draw Beta_Z
  mnStarZ <- XXX
  sdStarZ <- XXX
  betaCurZ <- rnorm(XXX,XXX,XXX)

  #Draw Beta_W
  mnStarW <- XXX
  sdStarW <- XXX
  betaCurW <- rnorm(XXX,XXX,XXX)
  
  #Draw Beta_M
  mnStarM <- XXX
  sdStarM <- XXX
  betaCurM <- rnorm(XXX,XXX,XXX)


  #Draw phi
  aStar <- n/2+p/2+a 

  bTemp1 <- .5*sum( (y-betaCur0-betaCurZ*x[,1]-betaCurW*x[,2]-betaCurM*x[,3])^2)
  bTemp2 <- .5*(1/g)*( (betaCur0-m0)^2+(betaCurZ-mZ)^2+(betaCurW-mW)^2+(betaCurM-mM)^2)
  bStar <- bTemp1+bTemp2+b  

  phiCur <- rgamma(1,aStar,bStar)
  
  #Store the draws
  betaStore[c,1] <- betaCur0
  betaStore[c,2] <- betaCurZ
  betaStore[c,3] <- betaCurW
  betaStore[c,4] <- betaCurM
  phiStore[c,1] <- phiCur
}  





