import numpy as np
import scipy.stats
import scipy.special
import matplotlib.pyplot as plt
import math

#%% 1(b)
fig, ax1 = plt.subplots(1, 1)
ax1.set_xlabel('y')

# Make the y-axis label and tick labels match the line color.
ax1.set_ylabel('number of data points', color='b')
for tl in ax1.get_yticklabels():
    tl.set_color('b')

# Make second y-axis
ax2 = ax1.twinx()
ax2.set_ylabel('Gamma pdf value', color='r')
for tl in ax2.get_yticklabels():
    tl.set_color('r')

# enter data and plot histogram on first y-axis
data = np.array([28, 16, 48, 16, 14, 35, 32])
ax1.hist(data, 4)
n = len(data)

# plot the gamma pdf on second y-axis
alpha = 9
beta = 9/np.mean(data)
x = np.arange(scipy.stats.gamma.ppf(0.0001, alpha, scale=1/beta), scipy.stats.gamma.ppf(0.9999, alpha, scale=1/beta))
ax2.plot(x, scipy.stats.gamma.pdf(x, alpha, scale=1/beta), 'r', ms=8, label='gamma pdf')

# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('1b.png') # save to same folder as .py file
manager.window.close() #close the plot window


#%% 1c
alpha = np.linspace(0, 15, 1000)
beta = 0.3
print(beta)
term3 = np.empty(len(alpha))
for index in range(len(alpha)):
	term3[index] = np.prod(np.power(data, alpha[index]-1))
likelihood = (np.power(beta, n*alpha) / np.power(scipy.special.gamma(alpha), n)) * term3 * math.exp(-beta*np.sum(data))
plt.figure()
plt.plot(alpha, likelihood)
plt.xlabel(r'$\alpha$')
plt.ylabel("likelihood")

# Full screen:
manager = plt.get_current_fig_manager()
manager.window.showMaximized()
plt.savefig('1c.png') # save to same folder as .py file
manager.window.close() #close the plot window

alpha_hat = alpha[np.nanargmax(likelihood)]
print(alpha_hat)


# plt.show()