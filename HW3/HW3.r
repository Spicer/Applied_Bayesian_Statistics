library(scatterplot3d)

# Download (from Canvas) the dataset called treeDat.csv.
# This dataset has three variables: Girth, Height, Volume.
#Load the data in your workspace
dat = read.table("treeDat.csv", header = TRUE, sep = ",")
#For the exercises that follow, we do only need the column
dat=dat[,2]
# To look at the data and make sure that the appropriate column is gone.
# Are there 31 observations?
dat

#LH function
lhFunc <- function(mu, phi, y, n)
{
  like <- (2*pi/phi)^(-n/2)*exp((-.5*phi)*sum((y-mu)^2))
  return(like)
}

# 3. Fix mu to equal 76 and plot the likelihood for phi between [0.0001; 0.1].
N <- 1000
mu <- 76
phiVec <- seq(.0001,.1, length=N)
lh <- rep(0,N)
for (i in 1:N)
{
  lh[i] <- lhFunc(mu, phiVec[i],dat, N)
}
plot(phiVec,lh, type="l")

# 4. Fix phi to equal 0.025 and plot the likelihood for mu between [70; 85].
N <- 1000
mu_vec <- seq(70,85, length=N)
phi  <- 0.025
lh <- rep(0,N)
for (i in 1:N)
{
  lh[i] <- lhFunc(mu_vec[i], phi, dat, 31)
}
plot(mu_vec, lh, type="l")

# 5. Now, calculate and plot the log-likelihood so that both mu and phi may vary. To do this,
# create a matrix parVec that has two columns. Each column contains values for mu and
# phi respectively:
N <- 1000
parVec <- matrix(0,N,2)
parVec[,1] <- runif(N, 70, 85)
parVec[,2] <- runif(N, .0001,.1)
# Now, run your function lhFunc N times.
lh <- rep(0,N)
for (i in 1:N)
{
  lh[i] <- lhFunc(parVec[i,1], parVec[i,2], dat, 31)
}
# Since the likelihood can get very close to zero, we'll plot the log-likelihood. Also, let's
# plot the point with maximum likelihood.
lhPlot <- scatterplot3d(parVec[,1], parVec[,2],log(lh),angle=45)
maxlh <- parVec[lh==max(lh)]
coord <- matrix(c(maxlh[1],maxlh[2],log(max(lh))),1,3)
lhPlot$points3d(coord, col="blue", pch=16)


# 10. Prior Sensitivity:
# x_i|theta ~ Bern(theta)
# theta ~ Beta(alpha, beta)
# 1. Create data
dat <- rbinom(10,1,.2)
# 2. Although, we set theta =Pr[xi = 1] to 0.2, what is the MLE for theta?
mle=sum(dat)/10
# 3. Plot the posterior distribution when alpha = 1 and beta = 1
thetaPost <- function(alpha,beta,dat,n)
{
  aStar <- alpha+sum(dat)
  bStar <- beta+n-sum(dat)
  thetaVec <- seq(0, 1, by=.01)
  post <- dbeta(thetaVec, aStar, bStar)
  plot(thetaVec, post, type="l", xlab="theta", ylab="Posterior Distribution")
  print("The posterior expectation is:")
  print(aStar / (aStar + bStar))
  print("The posterior variance is:")
  print((aStar * bStar) / ((aStar + bStar)^2 * (aStar + bStar + 1)))
}
thetaPost(1,1,dat,10)

# 4. Add to the function so that you print the posterior expectation and variance when you
# execute the function (see cheat sheet for formula). You must use the print() function.

# 5. Homework: Repeat three times: Change the prior parameters, alpha and beta and assess
# differences in the resulting posterior distribution by stating the posterior expectations
# and variances. Also, on one graph, plot the four postetrior distributions
# (given alpha = beta = 1 and your three, new prior specifcations).
par(mfrow=c(2,2))
alpha = c(1, 0.5, 0.75, 0.25)
beta = c(1, 0.5, 0.25, 0.75)
for (i in 1:4)
{
  thetaPost(alpha[i], beta[i], dat, 10)
}
