# To create a vector of numbers:
c(3,4,7) # concatenate

# To create an object which will be stored in your directory:
x <- c(3,4,7)

#You can use an `=' or an arrow `<-'.
#It does not matter which you choose. This lab uses `<-',
#but you are welcome to replace the arrows with `=', if you want.
#So, you can accomplish the same as above with the following notation
x = c(3,4,7)

# To remove an object (e.g., an object called temp) from your directory:
temp = 3
rm(temp)

# To view the first and third elements of x:
x[c(1,3)] # note that x[1,3] would fail
# in general, the round brackets ( ) are used in functions like c()

# An example of how to create a 2 3 matrix:
y <- matrix(c(1,2,3,4,3,4),2,3)

# The last two arguments of the matrix function are the number of rows and columns,
# respectively. If you would like the numbers to be entered by row instead of the
# default by column, add an additional argument, byrow=T.
# matrix(vector of data, numrows, numcols, byrow=T)
# To view the matrix, y:
print(y)

# To view the first row of y:
print(y[1,])

# To view the second and third columns of y:
print(y[,c(2,3)])

# To find help on any R function:
# help(function)
# For example, to find help on the function "matrix":
help(matrix)

# You can create your own functions using the following format:
#> newfunc <- function(args)
#{
#   commands
#}
# An example of a user written function to find the approximate
# derivative of a function f:
derivf <- function(x)
{
  (f(x+0.001) - f(x))/0.001
}

# Lets say that f has been defined as:
> f <- function(x)
{
  x^3 + x^2 + x + 10
}
# The derivative of f is obviously 3x^2 + 2x + 1.
# Lets evaluate this true derivative and the approximate derivative (using
# the function derivf) for several values of x:
x <- c(1:6) # this produces the vector (1,2,3,4,5,6)
3*x^2 + 2*x + 1 # this is the exact derivative
derivf(x) #this is the approximate derivative

# Lets create our original object vector, x:
x <- c(3,4,7)

x[-3] # this will provide all numbers in a vector except for the third
x[x >=5] # this will produce all numbers in x that are greater than or equal to 5

# Notice that arithmetic operations are applied to each element in a vector. For example:
x+3
# You can sum a vector
sum(x)
# You can take a product of a vector
prod(x)

# Let z be a new vector:
z <- c(1.5, 1/6,1/3)
# View only the first two decimal places of z:
round(z,2)

# View z in reverse order:
rev(z)
# Order z from smallest to largest:
sort(z)
# Determine the length of the vector z:
length(z)
# Find the maximum value of z in two ways:
max(z)
sort(z)[length(z)]

# Identify the ordering of z:
order(z)
# [1] 2 3 1 # i.e., the 2nd number is the min and the 1st number is the max

# Now, create a sequence of numbers:
seq(1,3,length=5)
# Create the same sequence in a slightly different way:
seq(1,3,by=0.5)
# Create another sequence by going from 3 to 1:
seq(3,1,by= -0.5)

# To randomly sample from an existing vector:
sample(x,10,replace=T)
# Or to randomly sample from a sequence of numbers from 1 to 500:
sample(1:500,10,replace=F)

# Missing values are represented by NA:
w<-c(3,4,NA,5)
# Some functions ignore NAs:
sort(w)
# Other function, by default, do not:
mean(w)
# Two ways to determine the mean of the non-missing values are:
mean(w,na.rm=T)
mean(w[!is.na(w)]) # where ! indicates "not" and is.na subsets only NA values
# Some functions, use "na.omit=T" instead of "na.rm=T"

# Create a new vector by concatenating x and z:
c(x,z)
# Create a matrix by treating x and z as rows:
rbind (x,z) # rbind binds by rows

# Create the same matrix, but, first treat x and z as columns:
t(cbind(x,z)) # cbind binds by columns

# R typically has four functions per distribution to
# 1) make random draws from the distribution,
# 2) calculate the area under the pdf or pmf,
# 3) solve for quantiles of the distribution, and
# 4) compute the pdf or pmf for values of the random variable.
# For example, type the following to get the help file referring to the normal distribution.
? dnorm
# Notice there are four functions. dnorm(), pnorm(), qnorm(), and rnorm().
# These functions are used as follows:
# For x~N(0,4), compute the pdf at x=1.3
# It is important to know the meaning of function arguments.
# R uses standard deviation, not variance, for the third function argument.
dnorm(1.3,mean=0, sd=2)
# Determine the area to the left of 0.4 for a normal (1.5,var=.16) random variable:
pnorm(.4,1.5,.4)
# Determine the quantile corresponding to the probability of 0.9 for a
# normal(0,1) random variable:
qnorm(0.9) #By default, R sets the mean to zero and variance to 1
# Generate three random numbers from a normal(0,9) distribution:
rnorm(3,0,3)
# Functions for other distributions work in a similar way. Remember that any manual
# can be accessed using the help( ) function. For example, if you want the syntax for
# the t distribution function, try help(rt)

# R has some very nice graphics capabilities. We'll only cover a minimal amount of
# information here.
# Lets create a mock simple linear regression data matrix:
regdata <- matrix(c(1:6,c(10,15,19,26,32,37)),6,2)
# Here, the first column is the explanatory variable and the second column is the
# response variable.
# To plot the data points:
plot(regdata[,1], regdata[,2]) # the x-axis variable is the first argument
# To plot the data points and have the points connected by lines:
plot(regdata[,1], regdata[,2], type="b")
# To plot only a line:
plot(regdata[,1], regdata[,2], type="l")
# To add meaningful axis labels and a title to the scatterplot:
plot(regdata[,1], regdata[,2], xlab="expl var", ylab="response",
       main="Regression data scatterplot")
# To set your own limits for the y-axis:
plot(regdata[,1], regdata[,2], ylim=c(0,40))
# To plot a histogram of uniform(0,1) random data:
hist(runif(1000, min=0, max=1))
hist(rnorm(1000, mean=0, sd=1))
# The function hist allows for several useful options. Type "help(hist)" for details.
# To plot the pdf of a normal(0,1) random variable:
plot(seq(-4,4,0.001), dnorm(seq(-4,4,0.001)), type="l")
# To view two graphs on the same plot region (i.e., page), one on top of the other:
par(mfrow=c(2,1))
plot(seq(-4,4,0.001), dnorm(seq(-4,4,0.001)), type="l")
plot(seq(-4,4,0.001), dnorm(seq(-4,4,0.001), mean=0, sd=0.75), type="l")
# To add a plot to a current figure use points or lines:
plot(seq(-4,4,0.001), dnorm(seq(-4,4,0.001)), type="l")
points(seq(-4,4,0.001), dnorm(seq(-4,4,0.001), mean=0, sd=0.75))
lines(seq(-4,4,0.001), dnorm(seq(-4,4,0.001), mean=0, sd=1.25))
# Enter "help(par)" to see the many options available for dealing with graphics. Many
# of the options (e.g., xlim, ylim) can be used directly with the plotting functions.

#Load the data in your workspace by writing in your source file
dat = read.table("delivTime2.csv", header = TRUE, sep = ",")
#There are a variety of ways to explore the data. Use some of
#the commands that you used above to do so.
#To remove columns (and rows) from the dataset, we can use a negative sign,
# e.g., remove the first column
dat=dat[,-1]
# To look at the data and make sure that the appropriate column is gone.
dat